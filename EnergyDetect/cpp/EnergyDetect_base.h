#ifndef ENERGYDETECT_IMPL_BASE_H
#define ENERGYDETECT_IMPL_BASE_H

#include <boost/thread.hpp>
#include <ossie/Resource_impl.h>
#include <ossie/ThreadedComponent.h>

#include <bulkio/bulkio.h>

class EnergyDetect_base : public Resource_impl, protected ThreadedComponent
{
    public:
        EnergyDetect_base(const char *uuid, const char *label);
        ~EnergyDetect_base();

        void start() throw (CF::Resource::StartError, CORBA::SystemException);

        void stop() throw (CF::Resource::StopError, CORBA::SystemException);

        void releaseObject() throw (CF::LifeCycle::ReleaseError, CORBA::SystemException);

        void loadProperties();

    protected:
        // Member variables exposed as properties
        CORBA::ULong differenceValue;
        float startSlopeThreshold;
        float endSlopeThreshold;
        float minBandwidth;
        float minDB;
        float bandwidthPerBin;
        float minChangePerBin;
        float endLevelPercent;

        // Ports
        bulkio::InFloatPort *dataFloat_in;
        bulkio::OutFloatPort *dataFloat_out;
        bulkio::OutFloatPort *debug_out;

    private:
};
#endif // ENERGYDETECT_IMPL_BASE_H
