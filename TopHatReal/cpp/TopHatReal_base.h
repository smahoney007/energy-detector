#ifndef TOPHATREAL_IMPL_BASE_H
#define TOPHATREAL_IMPL_BASE_H

#include <boost/thread.hpp>
#include <ossie/Resource_impl.h>
#include <ossie/ThreadedComponent.h>

#include <bulkio/bulkio.h>

class TopHatReal_base : public Resource_impl, protected ThreadedComponent
{
    public:
        TopHatReal_base(const char *uuid, const char *label);
        ~TopHatReal_base();

        void start() throw (CF::Resource::StartError, CORBA::SystemException);

        void stop() throw (CF::Resource::StopError, CORBA::SystemException);

        void releaseObject() throw (CF::LifeCycle::ReleaseError, CORBA::SystemException);

        void loadProperties();

    protected:
        // Member variables exposed as properties
        CORBA::Long DilateLength;
        CORBA::Long ErodeLength;
        CORBA::ULong operation;
        bool sendBotHat;
        bool sendClose;
        bool sendData;
        bool sendDilate;
        bool sendErode;
        bool sendOpen;
        bool sendTopHat;

        // Ports
        bulkio::InFloatPort *dataFloat_in;
        bulkio::OutFloatPort *dataFloat_out;

    private:
};
#endif // TOPHATREAL_IMPL_BASE_H
