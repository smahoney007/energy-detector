/**************************************************************************

    This is the component code. This file contains the child class where
    custom functionality can be added to the component. Custom
    functionality to the base class can be extended here. Access to
    the ports can also be done from this class

**************************************************************************/

#include "Average.h"

PREPARE_LOGGING(Average_i)

Average_i::Average_i(const char *uuid, const char *label) :
    Average_base(uuid, label)
{
  currentCount = 1;
}

Average_i::~Average_i()
{
}

int Average_i::serviceFunction()
{
    LOG_DEBUG(Average_i, "serviceFunction() example log message");
    
    bulkio::InFloatPort::dataTransfer *packetIn = dataFloat_in->getPacket(0);
    if (not packetIn) {
      return NOOP;
    }

    if(packetIn->inputQueueFlushed == true){
      LOG_ERROR(Average_i, "Dropped Packet");
    }

    //LOG_INFO(Average_i, "Received Packet size: " << packetIn->dataBuffer.size() << " mode: " << packetIn->SRI.mode << " id: " << packetIn->SRI.streamID);
    std::complex<float>* inputData = (std::complex<float>*)packetIn->dataBuffer.data();

    // input data is complex so divide by 2
    outputVector.resize(packetIn->dataBuffer.size() / 2);

/*
  // Element by Element averaging ////////////////////////////////////////////
    for(unsigned int loop = 0; loop < packetIn->dataBuffer.size() / 2; loop++){
      outputVector[loop] = outputVector[loop] + (std::abs(inputData[loop]) / numAverages);
    }

    std::string newId = "averages";
    if (packetIn->sriChanged) {
        //dataFloat_out->pushSRI(packetIn->SRI);
        outputSri = packetIn->SRI;
        outputSri.mode = 0;
        outputSri.streamID = newId.c_str();
        dataFloat_out->pushSRI(outputSri);
    }

    if (currentCount >= numAverages){

      dataFloat_out->pushPacket(outputVector.data(), 4096, packetIn->T, packetIn->EOS, newId);
      dataFloat_out->pushPacket(outputVector.data() + 4096, 4096, packetIn->T, packetIn->EOS, newId);

      memset(outputVector.data(), 0, sizeof(float) * outputVector.size());
      currentCount = 0;
    }
  ///////////////////////////////////////////////////////////////////////////////
*/
    // Exponential averaging ////////////////////////////////////////////
    //LOG_INFO(Average_i, "serviceFunction() example log message depth: " << dataFloat_in->getCurrentQueueDepth());

    for(unsigned int loop = 0; loop < packetIn->dataBuffer.size() / 2; loop++){
      outputVector[loop] = (std::abs(inputData[loop]) * alpha) + (outputVector[loop] * (1 - alpha));
    }

    std::string newId = "averages";
    if (packetIn->sriChanged) {
      //dataFloat_out->pushSRI(packetIn->SRI);
      outputSri = packetIn->SRI;
      outputSri.mode = 0;
      outputSri.streamID = newId.c_str();
      dataFloat_out->pushSRI(outputSri);
    }

    //LOG_INFO(Average_i, "insize: " << packetIn->dataBuffer.size() / 2 << " subsize: " << packetIn->SRI.subsize);
    for(int loop = 0; loop < ((int)packetIn->dataBuffer.size() / 2) / packetIn->SRI.subsize; loop++){
      //LOG_INFO(Average_i, "pushpacket start: " << (loop * packetIn->SRI.subsize) << " size: " << packetIn->SRI.subsize);
      dataFloat_out->pushPacket(outputVector.data() + (loop * packetIn->SRI.subsize), packetIn->SRI.subsize, packetIn->T, packetIn->EOS, newId);
    }

    //dataFloat_out->pushPacket(outputVector.data(), 4096, packetIn->T, packetIn->EOS, newId);
    //dataFloat_out->pushPacket(outputVector.data() + 4096, 4096, packetIn->T, packetIn->EOS, newId);

    ///////////////////////////////////////////////////////////////////////////////
/*
    // consecutive element averaging  /////////////////////////////////////////////
    memset(outputVector.data(), 0, sizeof(float) * outputVector.size());

    // set the beginning values
    for(unsigned int outer = 0; outer < numAverages / 2; outer++){
      outputVector[outer] = std::abs(inputData[outer]);
      //LOG_INFO(Average_i, "outer: " << outer);
    }

    //LOG_INFO(Average_i, "start: " << numAverages / 2 << " stop: " << (packetIn->dataBuffer.size() / 2) - (numAverages / 2));

    for(unsigned int outer = numAverages / 2; outer < (packetIn->dataBuffer.size() / 2) - (numAverages / 2); outer++){
      //LOG_INFO(Average_i, "start inner: " << outer - (numAverages / 2) << " stop: " << outer + (numAverages / 2));
      for(unsigned int inner = outer - (numAverages / 2); inner < outer + (numAverages / 2); inner++){
        //LOG_INFO(Average_i, "inner: " << inner << " plus: " << outer + inner);
        //LOG_INFO(Average_i, "outer: " << outer);
        outputVector[outer] = outputVector[outer] + (std::abs(inputData[inner]) / numAverages);
        //LOG_INFO(Average_i, "inner: " << inner << " plus: " << outer + inner);
      }
    }

    //LOG_INFO(Average_i, "start: " << (packetIn->dataBuffer.size() / 2) - numAverages / 2 << " stop: " << packetIn->dataBuffer.size() / 2);

    for(unsigned int loop = (packetIn->dataBuffer.size() / 2) - numAverages / 2; loop < packetIn->dataBuffer.size() / 2; loop++){
      outputVector[loop] = std::abs(inputData[loop]);
      //LOG_INFO(Average_i, "outer: " << loop);
    }

    std::string newId = "averages";
    if (packetIn->sriChanged) {
        //dataFloat_out->pushSRI(packetIn->SRI);
        outputSri = packetIn->SRI;
        outputSri.mode = 0;
        outputSri.streamID = newId.c_str();
        dataFloat_out->pushSRI(outputSri);
    }

    dataFloat_out->pushPacket(outputVector.data(), 4096, packetIn->T, packetIn->EOS, newId);
    dataFloat_out->pushPacket(outputVector.data() + 4096, 4096, packetIn->T, packetIn->EOS, newId);

    ///////////////////////////////////////////////////////////////////////////////
*/

    //dataFloat_out->pushPacket(packetIn->dataBuffer, packetIn->T, packetIn->EOS, packetIn->streamID);

    currentCount++;
    delete packetIn;
    return NORMAL;
}

