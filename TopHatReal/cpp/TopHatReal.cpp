/**************************************************************************

    This is the component code. This file contains the child class where
    custom functionality can be added to the component. Custom
    functionality to the base class can be extended here. Access to
    the ports can also be done from this class

**************************************************************************/

#include "TopHatReal.h"

PREPARE_LOGGING(TopHatReal_i)

TopHatReal_i::TopHatReal_i(const char *uuid, const char *label) :
    TopHatReal_base(uuid, label)
{

}

TopHatReal_i::~TopHatReal_i()
{
}


int TopHatReal_i::erode(std::vector<float>* Input, int ErodeElement, std::vector<float>* Output){

  if(Output->size() != Input->size() && !Input->empty()){
    return -1;
  }

  // just finding the max so it will be changed later
  // can be done in faster ways
  float theMax = Input->at(0);
  float vectorMin = Input->at(0);
  for(unsigned int loop = 0; loop < Input->size(); loop++){
    if(theMax < Input->at(loop)){
      theMax = Input->at(loop);
    }
    if(vectorMin > Input->at(loop)){
      vectorMin = Input->at(loop);
    }
  }

  float theMin = theMax;
  for(int outer = 0; outer < Input->size(); outer++){
    for(int inner = -1 * ErodeElement / 2; inner < ErodeElement / 2 - 1; inner++){
      if(outer + inner < 0 || outer + inner > Input->size() - 1){
        theMin = vectorMin;
      }
      else{
        if(theMin > Input->at(outer+inner)){
          theMin = Input->at(outer+inner);
          //LOG_INFO(TopHatReal_i, "New Min: " << theMin);
        }
      }
    }
    Output->at(outer) = theMin;
    theMin = theMax;
  }
  return 0;
}

int TopHatReal_i::dilate(std::vector<float>* Input, int DilateElement, std::vector<float>* Output){

  if(Output->size() != Input->size() && !Input->empty()){
    return -1;
  }

  // just finding the min so it will be changed later
  // can be done in faster ways
  float theMin = Input->at(0);
  for(unsigned int loop = 0; loop < Input->size(); loop++){
    if(theMin > Input->at(loop)){
      theMin = Input->at(loop);
    }
  }

  float theMax = theMin;
  for(int outer = 0; outer < Input->size(); outer++){
    for(int inner = -1 * DilateElement / 2 + 1; inner < DilateElement / 2; inner++){
      if(outer + inner < 0){
        theMax = Input->at(0);
      }
      else if(outer + inner > Input->size() - 1){
        theMax = Input->at(Input->size() - 1);
      }
      else{
        if(theMax < Input->at(outer+inner)){
          theMax = Input->at(outer+inner);
          //LOG_INFO(TopHatReal_i, "New Max: " << theMax);
        }
      }
    }
    Output->at(outer) = theMax;
    theMax = theMin;
  }
  return 0;
}


int TopHatReal_i::open(std::vector<float>* Input, int ErodeElement, int DilateElement, std::vector<float>* Output){
  std::vector<float> midVector;
  midVector.resize(Input->size());
  if(erode(Input, ErodeElement, &midVector) == 0){
    return dilate(&midVector, DilateElement, Output);
  }
  return -1;
}

int TopHatReal_i::close(std::vector<float>* Input, int ErodeElement, int DilateElement, std::vector<float>* Output){
  std::vector<float> midVector;
  midVector.resize(Input->size());
  if(dilate(Input, DilateElement, &midVector) == 0){
    return erode(&midVector, ErodeElement, Output);
  }
  return -1;
}

int TopHatReal_i::tophat(std::vector<float>* Input, int ErodeElement, int DilateElement, std::vector<float>* Output){
  if(open(Input, ErodeElement, DilateElement, Output) == 0){
    for(unsigned int loop = 0; loop < Input->size(); loop++){
      Output->at(loop) = Input->at(loop) - Output->at(loop);
    }
  }
  else{
    return -1;
  }
  return 0;
}

int TopHatReal_i::bothat(std::vector<float>* Input, int ErodeElement, int DilateElement, std::vector<float>* Output){
  if(close(Input, ErodeElement, DilateElement, Output) == 0){
    for(unsigned int loop = 0; loop < Input->size(); loop++){
      Output->at(loop) = Output->at(loop) - Input->at(loop);
    }
  }
  else{
    return -1;
  }
  return 0;
}

int TopHatReal_i::serviceFunction()
{
  LOG_DEBUG(TopHatReal_i, "serviceFunction() example log message");

  bulkio::InFloatPort::dataTransfer *packetIn = dataFloat_in->getPacket(0);
  if (not packetIn) {
    return NOOP;
  }

  ////// MULTI OUTPUT /////////////

  erodeVector.resize(packetIn->dataBuffer.size());
  dilateVector.resize(packetIn->dataBuffer.size());
  openVector.resize(packetIn->dataBuffer.size());
  closeVector.resize(packetIn->dataBuffer.size());
  tophatVector.resize(packetIn->dataBuffer.size());
  bothatVector.resize(packetIn->dataBuffer.size());

  if(sendErode){
    erode(&packetIn->dataBuffer, ErodeLength, &erodeVector);
  }
  if(sendDilate){
    dilate(&packetIn->dataBuffer, DilateLength, &dilateVector);
  }
  if(sendOpen){
    open(&packetIn->dataBuffer, ErodeLength, DilateLength, &openVector);
  }
  if(sendClose){
    close(&packetIn->dataBuffer, ErodeLength, DilateLength, &closeVector);
  }
  if(sendTopHat){
    bothat(&packetIn->dataBuffer, ErodeLength, DilateLength, &tophatVector);
  }
  if(sendBotHat){
    tophat(&packetIn->dataBuffer, ErodeLength, DilateLength, &bothatVector);
  }

  if(sendErode){
    std::string newId = "erode";
    outputSri = packetIn->SRI;
    outputSri.streamID = newId.c_str();
    dataFloat_out->pushSRI(outputSri);
    dataFloat_out->pushPacket(erodeVector, packetIn->T, packetIn->EOS, newId);
  }
  if(sendDilate){
    std::string newId = "dilate";
    outputSri = packetIn->SRI;
    outputSri.streamID = newId.c_str();
    dataFloat_out->pushSRI(outputSri);
    dataFloat_out->pushPacket(dilateVector, packetIn->T, packetIn->EOS, newId);
  }
  if(sendOpen){
    std::string newId = "open";
    outputSri = packetIn->SRI;
    outputSri.streamID = newId.c_str();
    dataFloat_out->pushSRI(outputSri);
    dataFloat_out->pushPacket(openVector, packetIn->T, packetIn->EOS, newId);
  }
  if(sendClose){
    std::string newId = "close";
    outputSri = packetIn->SRI;
    outputSri.streamID = newId.c_str();
    dataFloat_out->pushSRI(outputSri);
    dataFloat_out->pushPacket(closeVector, packetIn->T, packetIn->EOS, newId);
  }
  if(sendTopHat){
    std::string newId = "topHat";
    outputSri = packetIn->SRI;
    outputSri.streamID = newId.c_str();
    dataFloat_out->pushSRI(outputSri);
    dataFloat_out->pushPacket(tophatVector, packetIn->T, packetIn->EOS, newId);
  }
  if(sendBotHat){
    std::string newId = "botHat";
    outputSri = packetIn->SRI;
    outputSri.streamID = newId.c_str();
    dataFloat_out->pushSRI(outputSri);
    dataFloat_out->pushPacket(bothatVector, packetIn->T, packetIn->EOS, newId);
  }

  if (packetIn->sriChanged) {
      dataFloat_out->pushSRI(packetIn->SRI);
  }

  if(sendData){
    dataFloat_out->pushPacket(packetIn->dataBuffer, packetIn->T, packetIn->EOS, packetIn->streamID);
  }

  delete packetIn;

  /////////////////////////////////////



  //SINGLE OUTPUT
/*
  outputVector.resize(packetIn->dataBuffer.size());

  switch(operation){
  case 0:
    erode((std::vector<std::complex<float> >*)&packetIn->dataBuffer, ErodeLength, (std::vector<std::complex<float> >*)&outputVector);
    break;
  case 1:
    dilate((std::vector<std::complex<float> >*)&packetIn->dataBuffer, DilateLength, (std::vector<std::complex<float> >*)&outputVector);
    break;
  case 2:
    open((std::vector<std::complex<float> >*)&packetIn->dataBuffer, ErodeLength, DilateLength, (std::vector<std::complex<float> >*)&outputVector);
    break;
  case 3:
    close((std::vector<std::complex<float> >*)&packetIn->dataBuffer, ErodeLength, DilateLength, (std::vector<std::complex<float> >*)&outputVector);
    break;
  case 4:
    bothat((std::vector<std::complex<float> >*)&packetIn->dataBuffer, ErodeLength, DilateLength, (std::vector<std::complex<float> >*)&outputVector);
    break;
  case 5:
    tophat((std::vector<std::complex<float> >*)&packetIn->dataBuffer, ErodeLength, DilateLength, (std::vector<std::complex<float> >*)&outputVector);
    break;
  default:
    LOG_DEBUG(TopHatReal_i, "Outside Enumeration Range.");
  }

  std::string transformId = "transform";
  if (packetIn->sriChanged) {
      dataFloat_out->pushSRI(packetIn->SRI);
      outputSri = packetIn->SRI;
      outputSri.streamID = transformId.c_str();
      dataFloat_out->pushSRI(outputSri);
  }

  if(sendData){
    dataFloat_out->pushPacket(packetIn->dataBuffer, packetIn->T, packetIn->EOS, packetIn->streamID);
  }
  dataFloat_out->pushPacket(outputVector, packetIn->T, packetIn->EOS, transformId);

  delete packetIn;
*/
  return NORMAL;
}

