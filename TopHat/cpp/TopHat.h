#ifndef TOPHAT_IMPL_H
#define TOPHAT_IMPL_H

#include "TopHat_base.h"

class TopHat_i : public TopHat_base
{
    ENABLE_LOGGING
    public:
        TopHat_i(const char *uuid, const char *label);
        ~TopHat_i();

        int erode(std::vector<std::complex<float> >* Input, int ErodeElement, std::vector<std::complex<float> >* Output);
        int dilate(std::vector<std::complex<float> >* Input, int DilateElement, std::vector<std::complex<float> >* Output);
        int open(std::vector<std::complex<float> >* Input, int ErodeElement, int DilateElement, std::vector<std::complex<float> >* Output);
        int close(std::vector<std::complex<float> >* Input, int ErodeElement, int DilateElement, std::vector<std::complex<float> >* Output);
        int tophat(std::vector<std::complex<float> >* Input, int ErodeElement, int DilateElement, std::vector<std::complex<float> >* Output);
        int bothat(std::vector<std::complex<float> >* Input, int ErodeElement, int DilateElement, std::vector<std::complex<float> >* Output);

        int serviceFunction();

    protected:
        std::vector<float> outputVector;


        std::vector<float> erodeVector;
        std::vector<float> dilateVector;
        std::vector<float> openVector;
        std::vector<float> closeVector;
        std::vector<float> tophatVector;
        std::vector<float> bothatVector;


        BULKIO::StreamSRI outputSri;
};

#endif // TOPHAT_IMPL_H
