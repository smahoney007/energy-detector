#ifndef ENERGYDETECT_IMPL_H
#define ENERGYDETECT_IMPL_H

#include "EnergyDetect_base.h"

class EnergyDetect_i : public EnergyDetect_base
{
    ENABLE_LOGGING
    public:
        EnergyDetect_i(const char *uuid, const char *label);
        ~EnergyDetect_i();
        int serviceFunction();

    protected:
        std::vector<float> differenceVector;
        //std::vector<int> zeroCrossVector;
        std::vector<float> zeroCrossVector;

        std::vector<float> outputVector;

        BULKIO::StreamSRI outputSri;

};

#endif // ENERGYDETECT_IMPL_H
