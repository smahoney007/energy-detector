#ifndef TOPHATREAL_IMPL_H
#define TOPHATREAL_IMPL_H

#include "TopHatReal_base.h"

class TopHatReal_i : public TopHatReal_base
{
    ENABLE_LOGGING
    public:
        TopHatReal_i(const char *uuid, const char *label);
        ~TopHatReal_i();

        int erode(std::vector<float>* Input, int ErodeElement, std::vector<float>* Output);
        int dilate(std::vector<float>* Input, int DilateElement, std::vector<float>* Output);
        int open(std::vector<float>* Input, int ErodeElement, int DilateElement, std::vector<float>* Output);
        int close(std::vector<float>* Input, int ErodeElement, int DilateElement, std::vector<float>* Output);
        int tophat(std::vector<float>* Input, int ErodeElement, int DilateElement, std::vector<float>* Output);
        int bothat(std::vector<float>* Input, int ErodeElement, int DilateElement, std::vector<float>* Output);

        int serviceFunction();
    protected:
        std::vector<float> outputVector;

        std::vector<float> erodeVector;
        std::vector<float> dilateVector;
        std::vector<float> openVector;
        std::vector<float> closeVector;
        std::vector<float> tophatVector;
        std::vector<float> bothatVector;

        BULKIO::StreamSRI outputSri;
};

#endif // TOPHATREAL_IMPL_H
