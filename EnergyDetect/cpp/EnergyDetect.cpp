/**************************************************************************

    This is the component code. This file contains the child class where
    custom functionality can be added to the component. Custom
    functionality to the base class can be extended here. Access to
    the ports can also be done from this class

**************************************************************************/

#include "EnergyDetect.h"

PREPARE_LOGGING(EnergyDetect_i)

EnergyDetect_i::EnergyDetect_i(const char *uuid, const char *label) :
    EnergyDetect_base(uuid, label)
{

}

EnergyDetect_i::~EnergyDetect_i()
{
}

int EnergyDetect_i::serviceFunction()
{
  LOG_DEBUG(EnergyDetect_i, "serviceFunction() example log message");

  bulkio::InFloatPort::dataTransfer *packetIn = dataFloat_in->getPacket(0);
  if (not packetIn) {
    return NOOP;
  }

  if(packetIn->inputQueueFlushed == true){
    LOG_ERROR(EnergyDetect_i, "Dropped Packet");
  }

  //LOG_INFO(EnergyDetect_i, "Received Packet size: " << packetIn->dataBuffer.size() << " mode: " << packetIn->SRI.mode << " id: " << packetIn->SRI.streamID << " queue: " << dataFloat_in->getCurrentQueueDepth());

  // bad way to do complex vs real but done for coding speed
  if(packetIn->SRI.mode == 1){
    // all of the sizes are divided by 2 to account for complex numbers
    std::complex<float>* inputData = (std::complex<float>*)packetIn->dataBuffer.data();

    differenceVector.resize(packetIn->dataBuffer.size() / 2);
    zeroCrossVector.resize(packetIn->dataBuffer.size() / 2);

    outputVector.resize(packetIn->dataBuffer.size());
    memset(outputVector.data(), 0, sizeof(float) * outputVector.size());

    for(unsigned int loop = 0; loop < differenceVector.size() - differenceValue; loop++){
      differenceVector[loop] = std::abs(inputData[loop + differenceValue]) - std::abs(inputData[loop]);
    }

    // find the differential for the leftovers
    for(unsigned int loop = differenceVector.size() - differenceValue; loop < differenceVector.size(); loop++){
      differenceVector[loop] = std::abs(inputData[differenceVector.size() - 1]) - std::abs(inputData[loop]);
    }

    // mark the zero crossings
    for(unsigned int loop = 0; loop < differenceVector.size() - 1; loop++){
      if((differenceVector[loop] >= 0 && differenceVector[loop + 1] < 0) || (differenceVector[loop] > 0 && differenceVector[loop + 1] <= 0) ||
          (differenceVector[loop] <= 0 && differenceVector[loop + 1] > 0) || (differenceVector[loop] < 0 && differenceVector[loop + 1] >= 0)){
        zeroCrossVector[loop] = 1;
      }
    }

    // find the signals
    unsigned int firstCrossing = 0;
    unsigned int secondCrossing = 0;
    unsigned int thirdCrossing = 0;
    while (firstCrossing < zeroCrossVector.size()){
      //LOG_DEBUG(EnergyDetect_i, "firstCrossing: " << firstCrossing);
      // find the first 0 crossing which could be the start of a signal
      if(zeroCrossVector[firstCrossing] == 1){
        secondCrossing = firstCrossing + 1;
        // find the second 0 crossing which could be the center of a signal
        while(zeroCrossVector[secondCrossing] == 0 && secondCrossing < zeroCrossVector.size() - 1){
          secondCrossing++;
        }

        // find the local max between the two crossings
        float localMax = differenceVector[firstCrossing];
        for(unsigned int loop = firstCrossing + 1; loop <= secondCrossing; loop++){
          if(localMax < differenceVector[loop]){
            localMax = differenceVector[loop];
          }
        }

        // check for the start of a signal
        if(localMax > startSlopeThreshold){
          thirdCrossing = secondCrossing;

          float localMin;
          do{
            thirdCrossing++;
            // find the third 0 crossing which could be the end of a signal
            while(zeroCrossVector[thirdCrossing] == 0 && thirdCrossing < zeroCrossVector.size() - 1){
              thirdCrossing++;
            }

            localMin = differenceVector[secondCrossing];
            // find the local min between the two crossings
            for(unsigned int loop = secondCrossing + 1; loop <= thirdCrossing; loop++){
              if(localMin > differenceVector[loop]){
                localMax = differenceVector[loop];
              }
            }

          } while (localMin > endSlopeThreshold && thirdCrossing < zeroCrossVector.size() -1);

          // found the end of the signal so mark it in the vector
          std::complex<float>* outputData = (std::complex<float>*)outputVector.data();
          // stop short in marking the signal in case this is the start of the next signal
          for(unsigned int loop = firstCrossing; loop <= thirdCrossing - 2; loop++){
            outputData[loop] = inputData[secondCrossing];
          }

          firstCrossing = thirdCrossing;
        }
        else{
          firstCrossing = secondCrossing;
        }
      }
      else{
        firstCrossing++;
      }
    }

    std::string newId = "signals";
    if (packetIn->sriChanged) {
      dataFloat_out->pushSRI(packetIn->SRI);
      outputSri = packetIn->SRI;
      outputSri.mode = 0;
      outputSri.streamID = newId.c_str();
      dataFloat_out->pushSRI(outputSri);
    }

    //dataFloat_out->pushPacket(packetIn->dataBuffer, packetIn->T, packetIn->EOS, packetIn->streamID);
    dataFloat_out->pushPacket(outputVector, packetIn->T, packetIn->EOS, newId);
  }
  else{

/*
//////////////// Attempt 1 ///////////////////////////////////////////////////////////////
    // using a pointer to make the code look similar to the complex code
    float* inputData = packetIn->dataBuffer.data();

    differenceVector.resize(packetIn->dataBuffer.size());
    zeroCrossVector.resize(packetIn->dataBuffer.size());

    outputVector.resize(packetIn->dataBuffer.size());
    memset(outputVector.data(), 0, sizeof(float) * outputVector.size());

    for(unsigned int loop = 0; loop < differenceVector.size() - differenceValue; loop++){
      differenceVector[loop] = inputData[loop + differenceValue] - inputData[loop];
    }

    // find the differential for the leftovers
    for(unsigned int loop = differenceVector.size() - differenceValue; loop < differenceVector.size(); loop++){
      differenceVector[loop] = inputData[differenceVector.size() - 1] - inputData[loop];
    }

    // mark the zero crossings
    for(unsigned int loop = 0; loop < differenceVector.size() - 1; loop++){
      if((differenceVector[loop] >= 0 && differenceVector[loop + 1] < 0) || (differenceVector[loop] > 0 && differenceVector[loop + 1] <= 0) ||
          (differenceVector[loop] <= 0 && differenceVector[loop + 1] > 0) || (differenceVector[loop] < 0 && differenceVector[loop + 1] >= 0)){
        zeroCrossVector[loop] = 1;
      }
    }

    // find the signals
    unsigned int firstCrossing = 0;
    unsigned int secondCrossing = 0;
    unsigned int thirdCrossing = 0;
    while (firstCrossing < zeroCrossVector.size()){
      // find the first 0 crossing which could be the start of a signal
      if(zeroCrossVector[firstCrossing] == 1){
        secondCrossing = firstCrossing + 1;
        // find the second 0 crossing which could be the center of a signal
        while(zeroCrossVector[secondCrossing] == 0 && secondCrossing < zeroCrossVector.size() - 1){
          secondCrossing++;
        }

        // find the local max between the two crossings
        float localMax = differenceVector[firstCrossing];
        for(unsigned int loop = firstCrossing + 1; loop <= secondCrossing; loop++){
          if(localMax < differenceVector[loop]){
            localMax = differenceVector[loop];
          }
        }

        // check for the start of a signal
        if(localMax > startSlopeThreshold){
          thirdCrossing = secondCrossing;

          float localMin;
          do{
            thirdCrossing++;
            // find the third 0 crossing which could be the end of a signal
            while(zeroCrossVector[thirdCrossing] == 0 && thirdCrossing < zeroCrossVector.size() - 1){
              thirdCrossing++;
            }

            localMin = differenceVector[secondCrossing];
            // find the local min between the two crossings
            for(unsigned int loop = secondCrossing + 1; loop <= thirdCrossing; loop++){
              if(localMin > differenceVector[loop]){
                localMax = differenceVector[loop];
              }
            }

          } while (localMin > endSlopeThreshold && thirdCrossing < zeroCrossVector.size() -1);

          // found the end of the signal so mark it in the vector
          float* outputData = outputVector.data();
          // stop short in marking the signal in case this is the start of the next signal
          for(unsigned int loop = firstCrossing; loop <= thirdCrossing - 2; loop++){
            outputData[loop] = inputData[secondCrossing];
          }

          firstCrossing = thirdCrossing;
        }
        else{
          firstCrossing = secondCrossing;
        }
      }
      else{
        firstCrossing++;
      }
    }

    std::string newId;
    if (packetIn->sriChanged) {
      dataFloat_out->pushSRI(packetIn->SRI);
      outputSri = packetIn->SRI;
      //outputSri.streamID = newId.c_str();
      outputSri.streamID = "signals";
      dataFloat_out->pushSRI(outputSri);

      newId = "zeros";
      outputSri.streamID = newId.c_str();
      debug_out->pushSRI(outputSri);
      newId = "difference";
      outputSri.streamID = newId.c_str();
      debug_out->pushSRI(outputSri);
    }

    dataFloat_out->pushPacket(packetIn->dataBuffer, packetIn->T, packetIn->EOS, packetIn->streamID);
    //dataFloat_out->pushPacket(outputVector, packetIn->T, packetIn->EOS, "signals");

    //debug_out->pushPacket(zeroCrossVector, packetIn->T, packetIn->EOS, "zeros");
    debug_out->pushPacket(differenceVector, packetIn->T, packetIn->EOS, "difference");
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/
/*
//////////////// Attempt 2 ///////////////////////////////////////////////////////////////
    // using a pointer to make the code look similar to the complex code

    float* inputData = packetIn->dataBuffer.data();

    // cut in half to measure upswing and downswing
    differenceValue = (minBandwidth / packetIn->SRI.xdelta) / 2;

    differenceVector.resize(packetIn->dataBuffer.size());
    zeroCrossVector.resize(packetIn->dataBuffer.size());

    outputVector.resize(packetIn->dataBuffer.size());
    memset(outputVector.data(), 0, sizeof(float) * outputVector.size());

    for(unsigned int loop = 0; loop < differenceVector.size() - differenceValue; loop++){
      differenceVector[loop] = inputData[loop + differenceValue] - inputData[loop];
    }

    // find the differential for the leftovers
    for(unsigned int loop = differenceVector.size() - differenceValue; loop < differenceVector.size(); loop++){
      differenceVector[loop] = inputData[differenceVector.size() - 1] - inputData[loop];
    }

    // mark the zero crossings
    for(unsigned int loop = 0; loop < differenceVector.size() - 1; loop++){
      if((differenceVector[loop] >= 0 && differenceVector[loop + 1] < 0) || (differenceVector[loop] > 0 && differenceVector[loop + 1] <= 0) ||
          (differenceVector[loop] <= 0 && differenceVector[loop + 1] > 0) || (differenceVector[loop] < 0 && differenceVector[loop + 1] >= 0)){
        zeroCrossVector[loop] = 1;
      }
      else{
        zeroCrossVector[loop] = 0;
      }
    }

    // find the signals
    unsigned int firstCrossing = 0;
    unsigned int secondCrossing = 0;
    unsigned int thirdCrossing = 0;
    while (firstCrossing < zeroCrossVector.size()){
      // find the first 0 crossing which could be the start of a signal
      if(zeroCrossVector[firstCrossing] == 1){

        secondCrossing = firstCrossing + 1;

        while(zeroCrossVector[secondCrossing] == 0 && secondCrossing < zeroCrossVector.size() - 1){
          secondCrossing++;
        }

        // find the slope between the two and make sure they meet the bandwidth and snr requirements
        float dbGain = inputData[secondCrossing] - inputData[firstCrossing];
        unsigned int binsUp = secondCrossing - firstCrossing;


        if(dbGain >= minDB && binsUp >= differenceValue){
          //LOG_INFO(EnergyDetect_i, "Found: " << firstCrossing << " to " << secondCrossing);
          //LOG_INFO(EnergyDetect_i, "dbGain: " << dbGain << " binsUp: " << binsUp);


          thirdCrossing = secondCrossing;

          float dbGainDown;
          unsigned int binsDown;
          do{
            thirdCrossing++;
            // find the third 0 crossing which could be the end of a signal
            while(zeroCrossVector[thirdCrossing] == 0 && thirdCrossing < zeroCrossVector.size() - 1){
              thirdCrossing++;
            }

            dbGainDown = inputData[thirdCrossing] - inputData[secondCrossing];
            binsDown = thirdCrossing - secondCrossing;

            //LOG_INFO(EnergyDetect_i, "Third Peak: " << secondCrossing << " to " << thirdCrossing);
            //LOG_INFO(EnergyDetect_i, "dbGainDown: " << dbGainDown << " binsDown: " << binsDown);

          } while ((inputData[thirdCrossing] * .8 > inputData[firstCrossing] || binsDown <= differenceValue) && thirdCrossing < zeroCrossVector.size());

          LOG_INFO(EnergyDetect_i, "Found signal: " << firstCrossing << " to " << thirdCrossing);
          LOG_INFO(EnergyDetect_i, "signal snr: " << inputData[thirdCrossing] << " to " << inputData[firstCrossing]);
          LOG_INFO(EnergyDetect_i, "dbGainStart: " << dbGain << " binsUp: " << binsUp << " dbGainDown: " << dbGainDown << " binsDown: " << binsDown);

          // found the end of the signal so mark it in the vector
          float* outputData = outputVector.data();
          // stop short in marking the signal in case this is the start of the next signal
          for(unsigned int loop = firstCrossing; loop <= thirdCrossing - 2; loop++){
            // ideally the max should be found in the interval-- using this for now to not have to code more
            outputData[loop] = inputData[secondCrossing];
          }

          firstCrossing = thirdCrossing;
        }
        else{
          firstCrossing = secondCrossing;
        }
      }
      else{
        firstCrossing++;
      }
    }

    std::string newId;
    if (packetIn->sriChanged) {
      dataFloat_out->pushSRI(packetIn->SRI);
      outputSri = packetIn->SRI;
      //outputSri.streamID = newId.c_str();
      outputSri.streamID = "signals";
      dataFloat_out->pushSRI(outputSri);

      newId = "zeros";
      outputSri.streamID = newId.c_str();
      debug_out->pushSRI(outputSri);
      newId = "difference";
      outputSri.streamID = newId.c_str();
      debug_out->pushSRI(outputSri);
    }

    dataFloat_out->pushPacket(packetIn->dataBuffer, packetIn->T, packetIn->EOS, packetIn->streamID);
    dataFloat_out->pushPacket(outputVector, packetIn->T, packetIn->EOS, "signals");

    debug_out->pushPacket(zeroCrossVector, packetIn->T, packetIn->EOS, "zeros");
    debug_out->pushPacket(differenceVector, packetIn->T, packetIn->EOS, "difference");
    LOG_INFO(EnergyDetect_i, "Push Packet");
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/
/*
//////////////// Attempt 3 ///////////////////////////////////////////////////////////////
    // using a pointer to make the code look similar to the complex code

    float* inputData = packetIn->dataBuffer.data();

    // cut in half to measure upswing and downswing
    differenceValue = (minBandwidth / packetIn->SRI.xdelta) / 2;

    differenceVector.resize(packetIn->dataBuffer.size());
    zeroCrossVector.resize(packetIn->dataBuffer.size());

    outputVector.resize(packetIn->dataBuffer.size());
    memset(outputVector.data(), 0, sizeof(float) * outputVector.size());

    for(unsigned int loop = 0; loop < differenceVector.size() - differenceValue; loop++){
      differenceVector[loop] = inputData[loop + differenceValue] - inputData[loop];
    }

    // find the differential for the leftovers
    for(unsigned int loop = differenceVector.size() - differenceValue; loop < differenceVector.size(); loop++){
      differenceVector[loop] = inputData[differenceVector.size() - 1] - inputData[loop];
    }

    // mark the zero crossings
    for(unsigned int loop = 0; loop < differenceVector.size() - 1; loop++){
      if((differenceVector[loop] >= 0 && differenceVector[loop + 1] < 0) || (differenceVector[loop] > 0 && differenceVector[loop + 1] <= 0) ||
          (differenceVector[loop] <= 0 && differenceVector[loop + 1] > 0) || (differenceVector[loop] < 0 && differenceVector[loop + 1] >= 0)){
        zeroCrossVector[loop] = 1;
      }
      else{
        zeroCrossVector[loop] = 0;
      }
    }

    // find the signals
    unsigned int firstCrossing = 0;
    unsigned int secondCrossing = 0;
    unsigned int thirdCrossing = 0;
    while (firstCrossing < zeroCrossVector.size()){
      // find the first 0 crossing which could be the start of a signal
      if(zeroCrossVector[firstCrossing] == 1){

        secondCrossing = firstCrossing + 1;

        while(zeroCrossVector[secondCrossing] == 0 && secondCrossing < zeroCrossVector.size() - 1){
          secondCrossing++;
        }

        // find the local max between the two crossings
        float localMax = differenceVector[firstCrossing];
        for(unsigned int loop = firstCrossing + 1; loop <= secondCrossing; loop++){
          if(localMax < differenceVector[loop]){
            localMax = differenceVector[loop];
          }
        }

        // no dividing by dx because the difference vector wasnt divided by dx
        if(localMax > minDB){
          thirdCrossing = secondCrossing;

          float localMin;
          do{
            thirdCrossing++;
            // find the third 0 crossing which could be the end of a signal
            while(zeroCrossVector[thirdCrossing] == 0 && thirdCrossing < zeroCrossVector.size() - 1){
              thirdCrossing++;
            }

            localMin = differenceVector[secondCrossing];
            // find the local min between the two crossings
            for(unsigned int loop = secondCrossing + 1; loop <= thirdCrossing; loop++){
              if(localMin > differenceVector[loop]){
                localMax = differenceVector[loop];
              }
            }



          } while (localMin < (minDB / differenceValue) && thirdCrossing < zeroCrossVector.size());

          LOG_INFO(EnergyDetect_i, "Found signal: " << firstCrossing << " to " << thirdCrossing);

          // found the end of the signal so mark it in the vector
          float* outputData = outputVector.data();
          // stop short in marking the signal in case this is the start of the next signal
          for(unsigned int loop = firstCrossing; loop <= thirdCrossing - 2; loop++){
            // ideally the max should be found in the interval-- using this for now to not have to code more
            outputData[loop] = inputData[secondCrossing];
          }

          firstCrossing = thirdCrossing;
        }
        else{
          firstCrossing = secondCrossing;
        }
      }
      else{
        firstCrossing++;
      }
    }

    std::string newId;
    if (packetIn->sriChanged) {
      dataFloat_out->pushSRI(packetIn->SRI);
      outputSri = packetIn->SRI;
      //outputSri.streamID = newId.c_str();
      outputSri.streamID = "signals";
      dataFloat_out->pushSRI(outputSri);

      newId = "zeros";
      outputSri.streamID = newId.c_str();
      debug_out->pushSRI(outputSri);
      newId = "difference";
      outputSri.streamID = newId.c_str();
      debug_out->pushSRI(outputSri);
    }

    dataFloat_out->pushPacket(packetIn->dataBuffer, packetIn->T, packetIn->EOS, packetIn->streamID);
    dataFloat_out->pushPacket(outputVector, packetIn->T, packetIn->EOS, "signals");

    //debug_out->pushPacket(zeroCrossVector, packetIn->T, packetIn->EOS, "zeros");
    debug_out->pushPacket(differenceVector, packetIn->T, packetIn->EOS, "difference");
    LOG_INFO(EnergyDetect_i, "Push Packet");
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/
    //////////////// Attempt 4 ///////////////////////////////////////////////////////////////
    // using a pointer to make the code look similar to the complex code

    float* inputData = packetIn->dataBuffer.data();

    // cut in half to measure upswing and downswing
    //differenceValue = (minBandwidth / packetIn->SRI.xdelta) / 2;
    bandwidthPerBin  = packetIn->SRI.xdelta;
    minChangePerBin = minDB / (minBandwidth / bandwidthPerBin);

    differenceVector.resize(packetIn->dataBuffer.size());
    zeroCrossVector.resize(packetIn->dataBuffer.size());

    outputVector.resize(packetIn->dataBuffer.size());
    memset(outputVector.data(), 0, sizeof(float) * outputVector.size());

    for(unsigned int loop = 0; loop < differenceVector.size() - differenceValue; loop++){
      differenceVector[loop] = inputData[loop + differenceValue] - inputData[loop];
    }

    // find the differential for the leftovers
    for(unsigned int loop = differenceVector.size() - differenceValue; loop < differenceVector.size(); loop++){
      differenceVector[loop] = inputData[differenceVector.size() - 1] - inputData[loop];
    }

/*
    // mark the zero crossings
    for(unsigned int loop = 0; loop < differenceVector.size() - 1; loop++){
      if((differenceVector[loop] >= 0.0 && differenceVector[loop + 1] < 0.0) || (differenceVector[loop] > 0.0 && differenceVector[loop + 1] <= 0.0) ||
          (differenceVector[loop] <= 0.0 && differenceVector[loop + 1] > 0.0) || (differenceVector[loop] < 0.0 && differenceVector[loop + 1] >= 0.0)
          || (differenceVector[loop] == 0.0 && differenceVector[loop + 1] == 0.0)){
        zeroCrossVector[loop] = 1;
      }
      else{
        zeroCrossVector[loop] = 0;
      }
    }
*/

    // mark the zero crossings
    unsigned int loop = 0;
    while(loop < differenceVector.size() - 1){
      if(differenceVector[loop] > 0.0 && differenceVector[loop + 1] < 0.0){
        zeroCrossVector[loop] = 1;
      }
      else if(differenceVector[loop] < 0.0 && differenceVector[loop + 1] > 0.0){
        zeroCrossVector[loop] = 1;
      }
      else if(differenceVector[loop] > 0.0 && differenceVector[loop + 1] == 0.0){
        unsigned int inner = loop + 1;
        while(differenceVector[inner] == 0.0 && inner < differenceVector.size() - 1){
          inner++;
        }

        if(differenceVector[inner] < 0.0){
          zeroCrossVector[loop] = 1;
        }
        else{
          zeroCrossVector[loop] = 0;
        }
      }
      else if(differenceVector[loop] < 0.0 && differenceVector[loop + 1] == 0.0){
        unsigned int inner = loop + 1;
        while(differenceVector[inner] == 0.0 && inner < differenceVector.size() - 1){
          inner++;
        }
        if(differenceVector[inner] > 0.0){
          zeroCrossVector[loop] = 1;
        }
        else{
          zeroCrossVector[loop] = 0;
        }
      }
      // both == 0
      else{
        zeroCrossVector[loop] = 0;
      }
      loop++;
    }


    // find the signals
    // FIXME: starting at 1 to try to avoid a problem with having a 0 in the first bin -- should be fixed better later
    unsigned int firstCrossing = 1;
    unsigned int secondCrossing = 0;
    unsigned int thirdCrossing = 0;
    while (firstCrossing < zeroCrossVector.size()){
      // find the first 0 crossing which could be the start of a signal
      if(zeroCrossVector[firstCrossing] == 1){

        //LOG_INFO(EnergyDetect_i, "firstCross: " << firstCrossing);

        secondCrossing = firstCrossing;
        float dbGain;
        unsigned int binsUp;
        bool foundSignal = false;
        bool breakLoop = false;

        do{
          secondCrossing++;

          while(zeroCrossVector[secondCrossing] == 0 && secondCrossing < zeroCrossVector.size() - 1){
            secondCrossing++;
          }

          // find the slope between the two and make sure they meet the bandwidth and snr requirements
          dbGain = inputData[secondCrossing] - inputData[firstCrossing];
          binsUp = secondCrossing - firstCrossing;

          //LOG_INFO(EnergyDetect_i, "checking firstCross: " << firstCrossing << " to " << secondCrossing << " dbGain: " << dbGain << " binsUp: " << binsUp);

          if(minChangePerBin > (dbGain / binsUp) || secondCrossing >= zeroCrossVector.size()){
            breakLoop = true;
          }
          else{
            // check if at least half the minimum bandwidth is used and the minDB is there
            if(binsUp > ((minBandwidth / bandwidthPerBin) / 2) && dbGain > minDB){
              breakLoop = true;
              foundSignal = true;
            }
          }
        }while(breakLoop == false);

        if(foundSignal == true){
          thirdCrossing = secondCrossing;

          float dbGainDown;
          do{
            thirdCrossing++;
            // find the third 0 crossing which could be the end of a signal
            while(zeroCrossVector[thirdCrossing] == 0 && thirdCrossing < zeroCrossVector.size() - 1){
              thirdCrossing++;
            }

            dbGainDown = inputData[thirdCrossing] - inputData[secondCrossing];

            //LOG_INFO(EnergyDetect_i, "Third Peak: " << secondCrossing << " to " << thirdCrossing);
            //LOG_INFO(EnergyDetect_i, "dbGainDown: " << dbGainDown << " binsDown: " << binsDown);

          } while ((inputData[thirdCrossing] * endLevelPercent > inputData[firstCrossing] || (thirdCrossing - firstCrossing) * bandwidthPerBin < minBandwidth) && thirdCrossing < zeroCrossVector.size());

          //LOG_INFO(EnergyDetect_i, "Found signal: " << firstCrossing << " to " << thirdCrossing);
          //LOG_INFO(EnergyDetect_i, "signal snr: " << inputData[thirdCrossing] << " to " << inputData[firstCrossing]);
          //LOG_INFO(EnergyDetect_i, "dbGainStart: " << dbGain << " binsUp: " << binsUp << " dbGainDown: " << dbGainDown);

          // found the end of the signal so mark it in the vector
          float* outputData = outputVector.data();
          float peakValue = inputData[firstCrossing];

          for(unsigned int loop = firstCrossing; loop <= thirdCrossing - 2; loop++){
            if(peakValue < inputData[loop]){
              peakValue = inputData[loop];
            }
          }

          // stop short in marking the signal in case this is the start of the next signal
          for(unsigned int loop = firstCrossing; loop <= thirdCrossing - 2; loop++){
            // can use max value but using second cross for debug now
            outputData[loop] = peakValue;
            //outputData[loop] = inputData[secondCrossing];
          }

          firstCrossing = thirdCrossing;
        }
        else{
          firstCrossing++;
        }
      }
      else{
        firstCrossing++;
      }
    }

    std::string newId;
    if (packetIn->sriChanged) {
      dataFloat_out->pushSRI(packetIn->SRI);
      outputSri = packetIn->SRI;
      //outputSri.streamID = newId.c_str();
      outputSri.streamID = "signals";
      dataFloat_out->pushSRI(outputSri);

      newId = "zeros";
      outputSri.streamID = newId.c_str();
      debug_out->pushSRI(outputSri);
      newId = "difference";
      outputSri.streamID = newId.c_str();
      debug_out->pushSRI(outputSri);
    }

    dataFloat_out->pushPacket(packetIn->dataBuffer, packetIn->T, packetIn->EOS, packetIn->streamID);
    dataFloat_out->pushPacket(outputVector, packetIn->T, packetIn->EOS, "signals");

    //debug_out->pushPacket(zeroCrossVector, packetIn->T, packetIn->EOS, "zeros");
    debug_out->pushPacket(differenceVector, packetIn->T, packetIn->EOS, "difference");
    //LOG_INFO(EnergyDetect_i, "Push Packet");
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////

  }


  //LOG_INFO(EnergyDetect_i, "0: " << std::abs(inputData[0]));
  //LOG_INFO(EnergyDetect_i, "8: " << std::abs(inputData[differenceValue]));
  //LOG_INFO(EnergyDetect_i, "ans: " << std::abs(differenceVector[0]));

  //dataFloat_out->pushPacket(differenceVector, packetIn->T, packetIn->EOS, newId);
  //LOG_INFO(EnergyDetect_i, "End Function");
  delete packetIn;
  return NORMAL;
}

