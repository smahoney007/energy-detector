#ifndef AVERAGE_IMPL_H
#define AVERAGE_IMPL_H

#include "Average_base.h"

class Average_i : public Average_base
{
    ENABLE_LOGGING
    public:
        Average_i(const char *uuid, const char *label);
        ~Average_i();
        int serviceFunction();

    protected:
        unsigned int currentCount;
        std::vector<float> outputVector;

        BULKIO::StreamSRI outputSri;
};

#endif // AVERAGE_IMPL_H
