#include "EnergyDetect_base.h"

/*******************************************************************************************

    AUTO-GENERATED CODE. DO NOT MODIFY

    The following class functions are for the base class for the component class. To
    customize any of these functions, do not modify them here. Instead, overload them
    on the child class

******************************************************************************************/

EnergyDetect_base::EnergyDetect_base(const char *uuid, const char *label) :
    Resource_impl(uuid, label),
    ThreadedComponent()
{
    loadProperties();

    dataFloat_in = new bulkio::InFloatPort("dataFloat_in");
    addPort("dataFloat_in", dataFloat_in);
    dataFloat_out = new bulkio::OutFloatPort("dataFloat_out");
    addPort("dataFloat_out", dataFloat_out);
    debug_out = new bulkio::OutFloatPort("debug_out");
    addPort("debug_out", debug_out);
}

EnergyDetect_base::~EnergyDetect_base()
{
    delete dataFloat_in;
    dataFloat_in = 0;
    delete dataFloat_out;
    dataFloat_out = 0;
    delete debug_out;
    debug_out = 0;
}

/*******************************************************************************************
    Framework-level functions
    These functions are generally called by the framework to perform housekeeping.
*******************************************************************************************/
void EnergyDetect_base::start() throw (CORBA::SystemException, CF::Resource::StartError)
{
    Resource_impl::start();
    ThreadedComponent::startThread();
}

void EnergyDetect_base::stop() throw (CORBA::SystemException, CF::Resource::StopError)
{
    Resource_impl::stop();
    if (!ThreadedComponent::stopThread()) {
        throw CF::Resource::StopError(CF::CF_NOTSET, "Processing thread did not die");
    }
}

void EnergyDetect_base::releaseObject() throw (CORBA::SystemException, CF::LifeCycle::ReleaseError)
{
    // This function clears the component running condition so main shuts down everything
    try {
        stop();
    } catch (CF::Resource::StopError& ex) {
        // TODO - this should probably be logged instead of ignored
    }

    Resource_impl::releaseObject();
}

void EnergyDetect_base::loadProperties()
{
    addProperty(differenceValue,
                1,
                "differenceValue",
                "",
                "readwrite",
                "",
                "external",
                "configure");

    addProperty(startSlopeThreshold,
                0.2,
                "startSlopeThreshold",
                "",
                "readwrite",
                "",
                "external",
                "configure");

    addProperty(endSlopeThreshold,
                0.15,
                "endSlopeThreshold",
                "",
                "readwrite",
                "",
                "external",
                "configure");

    addProperty(minBandwidth,
                200.0,
                "minBandwidth",
                "",
                "readwrite",
                "",
                "external",
                "configure");

    addProperty(minDB,
                3.0,
                "minDB",
                "",
                "readwrite",
                "",
                "external",
                "configure");

    addProperty(bandwidthPerBin,
                0.0,
                "bandwidthPerBin",
                "",
                "readonly",
                "",
                "external",
                "configure");

    addProperty(minChangePerBin,
                0.0,
                "minChangePerBin",
                "",
                "readonly",
                "",
                "external",
                "configure");

    addProperty(endLevelPercent,
                0.8,
                "endLevelPercent",
                "",
                "readwrite",
                "",
                "external",
                "configure");

}


