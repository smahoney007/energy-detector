#ifndef TOPHAT_IMPL_BASE_H
#define TOPHAT_IMPL_BASE_H

#include <boost/thread.hpp>
#include <ossie/Resource_impl.h>
#include <ossie/ThreadedComponent.h>

#include <bulkio/bulkio.h>

class TopHat_base : public Resource_impl, protected ThreadedComponent
{
    public:
        TopHat_base(const char *uuid, const char *label);
        ~TopHat_base();

        void start() throw (CF::Resource::StartError, CORBA::SystemException);

        void stop() throw (CF::Resource::StopError, CORBA::SystemException);

        void releaseObject() throw (CF::LifeCycle::ReleaseError, CORBA::SystemException);

        void loadProperties();

    protected:
        // Member variables exposed as properties
        CORBA::Long ErodeLength;
        CORBA::Long DilateLength;
        CORBA::ULong operation;
        bool sendData;
        bool sendDilate;
        bool sendErode;
        bool sendOpen;
        bool sendClose;
        bool sendTopHat;
        bool sendBotHat;

        // Ports
        bulkio::InFloatPort *dataFloat_in;
        bulkio::OutFloatPort *dataFloat_out;

    private:
};
#endif // TOPHAT_IMPL_BASE_H
